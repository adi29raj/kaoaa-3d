   Live Link : https://kaoaa-3d-adi29raj-57d7571da263fa5f2429fc83f84311e21d929cbaab7bc.gitlab.io

1. Players decide who will play the vulture, and who will play the crows.

2. The board is empty in the beginning. All pieces are set beside the board.

3. Players alternate their turns throughout the game.

4. Drop phase: Crows start first, and one crow is dropped anywhere on the board. Crows continue to drop one piece per turn on any vacant point until all seven crows have been dropped which requires seven turns. Afterwards, the crows can begin to move.

5. After the first crow is dropped, the vulture is dropped by the other player on any vacant point on the board. On the vulture's next turn, it can move and start capturing crows.

6. Move phase: After all seven crows have been dropped, the crows can move one space onto a vacant point per turn following the pattern on the board. Crows cannot capture.